package com.cnam.secondlifeapp.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Query;
import androidx.room.Transaction;

import com.cnam.secondlifeapp.model.relationBetweenTable.ProductAndProposal;

import java.util.List;

public interface RelationBetweenTableDao {
    @Transaction
    @Query("SELECT * FROM Product")
    List<ProductAndProposal> getProductsAndProposals();

}
