package com.cnam.secondlifeapp.Dao;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.cnam.secondlifeapp.model.Game;
import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.model.User;

import java.util.List;

@Dao
public interface GameDao {
    @Query("SELECT * FROM game")
    LiveData<List<Game>> getAll();

    @Query("SELECT * FROM game WHERE id IN (:id)")
    LiveData<Game> findById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Game... game);

    @Update
    void update(Game game);

    @Delete
    void delete(Game game);

    @Query("DELETE FROM game")
    void deleteAll();
}
