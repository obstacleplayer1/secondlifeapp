package com.cnam.secondlifeapp.Dao;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.cnam.secondlifeapp.model.Opinion;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.model.User;

import java.util.List;

@Dao
public interface ProposalDao {
    @Query("SELECT * FROM proposal")
    LiveData<List<Proposal>> getAll();

    @Query("SELECT * FROM proposal WHERE id IN (:id)")
    LiveData<Proposal> findById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Proposal... proposal);

    @Update
    void update(Proposal proposal);

    @Delete
    void delete(Proposal proposal);

    @Query("DELETE FROM proposal")
    void deleteAll();

}
