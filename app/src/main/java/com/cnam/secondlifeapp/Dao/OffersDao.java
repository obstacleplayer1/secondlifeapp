package com.cnam.secondlifeapp.Dao;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Opinion;
import com.cnam.secondlifeapp.model.User;

import java.util.List;

@Dao
public interface OffersDao {
    @Query("SELECT * FROM offer")
    LiveData<List<Offer>> getAll();

    @Query("SELECT * FROM offer WHERE id IN (:id)")
    LiveData<Offer> findById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Offer... offer);

    @Update
    void update(Offer offer);

    @Delete
    void delete(Offer offer);

    @Query("DELETE FROM offer")
    void deleteAll();
}
