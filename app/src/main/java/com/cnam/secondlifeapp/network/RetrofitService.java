package com.cnam.secondlifeapp.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class RetrofitService {

    public static Retrofit Create(){

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new Retrofit.Builder()
                .baseUrl("http://nas.fuchsh.fr:32775/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static JsonPlaceHolderApi auth(){
        return Create().create(JsonPlaceHolderApi.class);
    }
}
