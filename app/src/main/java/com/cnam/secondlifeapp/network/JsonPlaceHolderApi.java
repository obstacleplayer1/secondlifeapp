package com.cnam.secondlifeapp.network;
import com.cnam.secondlifeapp.model.Game;

import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.model.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface JsonPlaceHolderApi {

    @GET("test")
    Call<ResponseBody> createGet();

    @POST("auth/signup")
    Call<RegisterResponse> createUser(@Body RegisterRequest registerRequest);

    @POST("auth/signin")
    Call<LoginResponse> loginUser(@Body LoginRequest loginRequest);

    @GET("auth/whoami")
    Call<ProfileResponse> profilInfo(@Header("Authorization") String auth);

    @GET("offers")
    Call<List<Offer>> getOffer(@Header("Authorization") String auth);

    @PUT("auth/account")
    Call<Void> profilModif(@Header("Authorization") String auth, @Body User user);

    @GET("offers/beingsold")
    Call<List<Offer>> getSales(@Header("Authorization") String auth);

    @GET("offers/beingpurchased")
    Call<List<Offer>> getListPurchase(@Header("Authorization") String auth);

    @DELETE("offers/{id}")
    Call<Offer> deleteSales(@Header("Authorization") String auth,@Path("id") int id);

    @POST("offers")
    Call<Offer> createOffer(@Header("Authorization") String auth, @Body Offer offers);

    @POST("offers/proposal")
    Call<Proposal> createProposal(@Header("Authorization") String auth,@Body Proposal proposal);

    @GET("offers/{id}/proposals")
    Call<List<Proposal>> myProposalOfferById(@Path("id") int offersId, @Header("Authorization") String auth);

    @GET("offers/proposals/{id}/decline")
    Call<Proposal> cancelProposal(@Header("Authorization") String auth,@Path("id") int id);

    @POST("offers/proposals/{id}/accept")
    Call<Proposal> acceptProposal(@Header("Authorization") String auth,@Path("id") int id,@Body String message);

    @GET("game")
    Call<List<Game>> getGames();




}