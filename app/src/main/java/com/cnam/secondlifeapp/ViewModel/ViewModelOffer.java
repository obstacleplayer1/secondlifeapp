package com.cnam.secondlifeapp.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.repository.OfferRepository;

import java.util.List;

public class ViewModelOffer extends AndroidViewModel {
    private OfferRepository offerRepository;
    private LiveData<List<Offer>> allOffer;

    public ViewModelOffer(@NonNull Application application) {
        super(application);
        offerRepository = new OfferRepository(application);
        allOffer = offerRepository.getAllOffer();
    }

    public void insert(Offer offer){
        offerRepository.insert(offer);
    }

    public void update(Offer offer){
        offerRepository.update(offer);
    }

    public void delete(Offer offer){
        offerRepository.delete(offer);
    }

    public void deleteAllUser(){
        offerRepository.deleteAll();
    }

    public LiveData<List<Offer>> getAllOffer(){
        return allOffer;
    }

}
