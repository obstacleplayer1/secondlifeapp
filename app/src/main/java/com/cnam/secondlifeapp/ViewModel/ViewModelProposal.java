package com.cnam.secondlifeapp.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.repository.ProposalRepository;

import java.util.List;

public class ViewModelProposal extends AndroidViewModel {

    private ProposalRepository proposalRepository;
    private LiveData<List<Proposal>> allProposal;

    public ViewModelProposal(@NonNull Application application) {
        super(application);
        proposalRepository = new ProposalRepository(application);
        allProposal = proposalRepository.getAllProposal();
    }

    public void insert(Proposal proposal){
        proposalRepository.insert(proposal);
    }

    public void update(Proposal proposal){
        proposalRepository.update(proposal);
    }

    public void delete(Proposal proposal){
        proposalRepository.delete(proposal);
    }

    public void deleteAllUser(){
        proposalRepository.deleteAll();
    }

    public LiveData<List<Proposal>> getAllUser(){
        return allProposal;
    }
}
