package com.cnam.secondlifeapp.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DecisionViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public DecisionViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Decision Fragment");
    }

}
