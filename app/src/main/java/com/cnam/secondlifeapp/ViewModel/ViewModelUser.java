package com.cnam.secondlifeapp.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.model.User;
import com.cnam.secondlifeapp.repository.UserRepository;

import java.util.List;

public class ViewModelUser extends AndroidViewModel {
    private UserRepository userRepository;
    private LiveData<List<User>> allUser;

    public ViewModelUser(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
        allUser = userRepository.getAllUsers();
    }

    public void insert(User user){
        userRepository.insert(user);
    }

    public void update(User user){
        userRepository.update(user);
    }

    public void delete(User user){
        userRepository.delete(user);
    }

    public void deleteAllUser(){
        userRepository.deleteAll();
    }

    public LiveData<List<User>> getAllUser(){
        return allUser;
    }

}
