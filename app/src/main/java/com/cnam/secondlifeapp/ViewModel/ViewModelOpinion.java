package com.cnam.secondlifeapp.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.model.Opinion;
import com.cnam.secondlifeapp.model.User;
import com.cnam.secondlifeapp.repository.OpinionRepository;
import com.cnam.secondlifeapp.repository.UserRepository;

import java.util.List;

public class ViewModelOpinion extends AndroidViewModel {
    private OpinionRepository opinionsRepository;
    private LiveData<List<Opinion>> allOpinion;

    public ViewModelOpinion(@NonNull Application application) {
        super(application);
        opinionsRepository = new OpinionRepository(application);
        allOpinion = opinionsRepository.getAllOpinion();
    }

    public void insert(Opinion opinions){
        opinionsRepository.insert(opinions);
    }

    public void update(Opinion opinions){
        opinionsRepository.update(opinions);
    }

    public void delete(Opinion opinions){
        opinionsRepository.delete(opinions);
    }

    public void deleteAllUser(){
        opinionsRepository.deleteAll();
    }

    public LiveData<List<Opinion>> getAllOpinion(){
        return allOpinion;
    }
}
