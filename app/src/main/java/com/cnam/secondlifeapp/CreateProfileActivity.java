package com.cnam.secondlifeapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cnam.secondlifeapp.network.RegisterRequest;
import com.cnam.secondlifeapp.network.RegisterResponse;
import com.cnam.secondlifeapp.network.RetrofitService;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateProfileActivity extends AppCompatActivity{

    TextInputLayout username;
    TextInputLayout password;
    TextInputLayout password2;
    TextInputLayout phonenumber;
    TextInputLayout streetnumber;
    TextInputLayout street;
    TextInputLayout city;
    TextInputLayout postalcode;
    TextInputLayout mail;

    Button buttonsubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createprofile);

        Button buttonBack = findViewById(R.id.ButtonBack);
        buttonsubmit = findViewById(R.id.ButtonCreateProfile);
        mail = findViewById(R.id.editTextCreateEmailAddress);
        username = findViewById(R.id.editTextCreatePseudo);
        password = findViewById(R.id.editTextCreatePassword);
        password2 = findViewById(R.id.editTextCreateConfirmPassword);
        phonenumber = findViewById(R.id.editTextCreatePhoneNumber);
        streetnumber = findViewById(R.id.editTextCreateStreetNumber);
        street = findViewById(R.id.editTextCreateStreet);
        city = findViewById(R.id.editTextCreateCity);
        postalcode = findViewById(R.id.editTextCreatePostalCode);

        buttonBack.setOnClickListener(v -> {
            Intent intent = new Intent(this, com.cnam.secondlifeapp.LoginActivity.class);
            startActivity(intent);
        });

        new  Thread(() -> buttonsubmit.setOnClickListener(v -> {
            if(TextUtils.isEmpty(username.getEditText().getText().toString())    || TextUtils.isEmpty(password.getEditText().getText().toString())    ||
                    TextUtils.isEmpty(mail.getEditText().getText().toString())        || TextUtils.isEmpty(password2.getEditText().getText().toString())   ||
                    TextUtils.isEmpty(phonenumber.getEditText().getText().toString()) || TextUtils.isEmpty(streetnumber.getEditText().getText().toString())||
                    TextUtils.isEmpty(street.getEditText().getText().toString())      || TextUtils.isEmpty(city.getEditText().getText().toString())        ||
                    TextUtils.isEmpty(postalcode.getEditText().getText().toString()))
            {
                Toast.makeText(CreateProfileActivity.this, "Champs incomplet(s)", Toast.LENGTH_LONG).show();
            }
            else {
                register();
            }
        })).start();

    }

    public void register() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername(username.getEditText().getText().toString());
        registerRequest.setPassword(password.getEditText().getText().toString());
        registerRequest.setMail(mail.getEditText().getText().toString());
        registerRequest.setPhonenumber(phonenumber.getEditText().getText().toString());
        registerRequest.setStreetnumber(streetnumber.getEditText().getText().toString());
        registerRequest.setStreet(street.getEditText().getText().toString());
        registerRequest.setCity(city.getEditText().getText().toString());
        registerRequest.setPostalcode(postalcode.getEditText().getText().toString());

        Call<RegisterResponse> registerResponseCall = RetrofitService.auth().createUser(registerRequest);

        registerResponseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(CreateProfileActivity.this, "Vous êtes inscrit", Toast.LENGTH_LONG).show();
                    RegisterResponse registerResponse = response.body();

                    startActivity(new Intent(CreateProfileActivity.this, LoginActivity.class));

                }else{
                    Toast.makeText(CreateProfileActivity.this,"L'email ou le pseudo est déjà utilisé", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                //Toast.makeText(CreateProfileActivity.this,"Throwable "+t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                Toast.makeText(CreateProfileActivity.this,"Problème de connexion", Toast.LENGTH_LONG).show();
            }
        });
    }


}
