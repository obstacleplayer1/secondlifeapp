package com.cnam.secondlifeapp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ResetPasswordActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resetpassword);

        Button buttonBack = findViewById(R.id.ButtonBack);

        buttonBack.setOnClickListener(v -> {

            Intent intent = new Intent(this, com.cnam.secondlifeapp.LoginActivity.class);
            startActivity(intent);

        });

    }


}
