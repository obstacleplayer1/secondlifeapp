package com.cnam.secondlifeapp.model.relationBetweenTable;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.model.Proposal;

public class ProductAndProposal {
    @Embedded
    public Product product;
    @Relation(
            parentColumn = "id",
            entityColumn = "id"
    )
    public Proposal proposal;

}
