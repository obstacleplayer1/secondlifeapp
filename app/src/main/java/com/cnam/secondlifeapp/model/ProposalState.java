package com.cnam.secondlifeapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public enum ProposalState implements Serializable {
    @SerializedName("0")
    Waiting,
    @SerializedName("1")
    Accepted,
    @SerializedName("2")
    Refused,
    @SerializedName("3")
    Canceled
}
