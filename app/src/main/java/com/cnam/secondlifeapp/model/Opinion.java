package com.cnam.secondlifeapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.cnam.secondlifeapp.model.User;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class Opinion implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @SerializedName("requestingUser")
    @ColumnInfo(name = "requestingUser")
    public User requestingUser;

    @SerializedName("targetedUser")
    @ColumnInfo(name = "targetedUser")
    public User targetedUser;

    @SerializedName("comment")
    @ColumnInfo(name = "comment")
    public String comment;

    @SerializedName("note")
    @ColumnInfo(name = "note")
    public int note;

    public Opinion(int id, User requestingUser, User targetedUser, String comment, int note) {
        this.id = id;
        this.requestingUser = requestingUser;
        this.targetedUser = targetedUser;
        this.comment = comment;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getRequestingUser() {
        return requestingUser;
    }

    public void setRequestingUser(User requestingUser) {
        this.requestingUser = requestingUser;
    }

    public User getTargetedUser() {
        return targetedUser;
    }

    public void setTargetedUser(User targetedUser) {
        this.targetedUser = targetedUser;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }
}
