package com.cnam.secondlifeapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;

import com.cnam.secondlifeapp.JsonConverter.JsonConverter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

@Entity
public class Proposal implements Serializable {


    @PrimaryKey(autoGenerate = true)
    private int id;



    @SerializedName("offer")
    @ColumnInfo(name = "offer")
    private Offer offer;

    @SerializedName("requestingUser")
    @ColumnInfo(name = "requestingUser")
    //@Embedded
    private User requestingUser;

    @SerializedName("proposedProducts")
    @ColumnInfo(name = "proposedProduct")
    private List<Product> proposedProduct;

    @SerializedName("proposedPrice")
    @ColumnInfo(name = "proposedPrice")
    private float proposedPrice;

    @SerializedName("sellerComment")
    @ColumnInfo(name = "sellerComment")
    private String sellerComment;

    @SerializedName("proposalState")
    @ColumnInfo(name = "proposalState")
    private ProposalState proposalState;



    public Proposal(int id, Offer offer, User requestingUser, List<Product> proposedProduct, float proposedPrice, String sellerComment, ProposalState proposalState) {
        this.id = id;
        this.offer = offer;
        this.requestingUser = requestingUser;
        this.proposedProduct = proposedProduct;
        this.proposedPrice = proposedPrice;
        this.sellerComment = sellerComment;
        this.proposalState = proposalState;
    }

    @Ignore
    public Proposal(Offer offer, List<Product> proposedProduct, float proposedPrice) {
        this.offer = offer;
        this.proposedProduct = proposedProduct;
        this.proposedPrice = proposedPrice;
    }

    @Ignore
    public Proposal() {
    }







    public int getId() {
        return id;
    }

    public void setId(int id) {
        id = id;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public User getRequestingUser() {
        return requestingUser;
    }

    public void setRequestingUser(User requestingUser) {
        this.requestingUser = requestingUser;
    }

    public List<Product> getProposedProduct() {
        return proposedProduct;
    }

    public void setProposedProduct(List<Product> proposedProduct) {
        this.proposedProduct = proposedProduct;
    }

    public float getProposedPrice() {
        return proposedPrice;
    }

    public void setProposedPrice(float proposedPrice) {
        this.proposedPrice = proposedPrice;
    }



    public String getSellerComment() {
        return sellerComment;
    }

    public void setSellerComment(String sellerComment) {
        this.sellerComment = sellerComment;
    }


    public ProposalState getProposalState() {
        return proposalState;
    }

    public void setProposalState(ProposalState proposalState) {
        this.proposalState = proposalState;
    }
}
