package com.cnam.secondlifeapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Offer implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("title")
    @ColumnInfo(name = "title")
    private String title;
    @SerializedName("description")
    @ColumnInfo(name = "description")
    private String description;

    //Prix de base demandé pour le lot
    @SerializedName("basePrice")
    @ColumnInfo(name = "basePrice")
    private float basePrice;

    //Liste des produits mis en vente par le vendeur
    @SerializedName("products")
    @ColumnInfo(name = "products")
    private List<Product> products;

    //L'utilisateur à qui appartient l'offre
    @SerializedName("seller")
    @ColumnInfo(name = "seller")
    private User seller;

    //L'utilisateur à qui revient l'offre après acceptation de sa proposition
    @SerializedName("buyer")
    @ColumnInfo(name = "buyer")
    private User buyer;

    //Liste des propositions effectuées par les acheteurs
    @SerializedName("proposals")
    @ColumnInfo(name = "proposals")
    public List<Proposal> proposals;

    public Offer(int id, String title, String description, float basePrice, List<Product> products, User seller, User buyer, List<Proposal> proposals) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.basePrice = basePrice;
        this.products = products;
        this.seller = seller;
        this.buyer = buyer;
        this.proposals = proposals;
    }

    @Ignore
    public Offer() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(float basePrice) {
        this.basePrice = basePrice;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public List<Proposal> getProposals() {
        return proposals;
    }

    public void setProposals(List<Proposal> proposals) {
        this.proposals = proposals;
    }
}