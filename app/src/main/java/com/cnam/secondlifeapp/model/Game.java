package com.cnam.secondlifeapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class Game implements Serializable {

    @ColumnInfo
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("name")
    @ColumnInfo(name = "name")
    private String name;

    @SerializedName("score")
    @ColumnInfo(name = "score")
    private float score;

    @SerializedName("description")
    @ColumnInfo(name = "description")
    private String description;

    @SerializedName("pictureUrl")
    @ColumnInfo(name = "pictureUrl")
    private String pictureUrl;

    public Game(int id, String name, float score, String description, String pictureUrl) {
        this.id = id;
        this.name = name;
        this.score = score;
        this.description = description;
        this.pictureUrl = pictureUrl;
    }

    @Ignore
    public Game(int id,float score) {
        this.id = id;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @Override
    public String toString() {
        return name;
    }
}
