package com.cnam.secondlifeapp.JsonConverter;

import androidx.room.TypeConverter;

import java.lang.reflect.Type;
import java.util.List;

import com.cnam.secondlifeapp.model.Game;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.model.ProposalState;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class JsonConverter {

    private static Gson gson;

    @TypeConverter
    public static List<Product> stringtoSomeObjectList(String data){

        Type listType = new TypeToken<List<Product>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObject(List<Product> dataObject){
        return gson.toJson(dataObject);
    }

    @TypeConverter
    public static User userObject(String data){

        Type usertype = new TypeToken<User>() {}.getType();
        return gson.fromJson(data, usertype);
    }


    @TypeConverter
    public static String UserObject(User dataObject){
        Gson gson = new Gson();
        String json = gson.toJson(dataObject);
        return json;
    }

    @TypeConverter
    public static List<Proposal> ProposalObject(String data){

        Type listType = new TypeToken<List<Proposal>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String ProposalObjectToJson(List<Proposal> dataObject){
        return gson.toJson(dataObject);
    }

    @TypeConverter
    public static Offer offerObject(String data){

        Type usertype = new TypeToken<Offer>() {}.getType();
        return gson.fromJson(data, usertype);
    }


    @TypeConverter
    public static String offerToJson(Offer dataObject){
        Gson gson = new Gson();
        String json = gson.toJson(dataObject);
        return json;
    }



    @TypeConverter
    public static Game GameObject(String data){

        Type usertype = new TypeToken<Game>() {}.getType();
        return gson.fromJson(data, usertype);
    }


    @TypeConverter
    public static String GameToJson(Game dataObject){
        Gson gson = new Gson();
        String json = gson.toJson(dataObject);
        return json;
    }

   @TypeConverter
    public static ProposalState proposalState(String data){

        Type usertype = new TypeToken<ProposalState>() {}.getType();
        return gson.fromJson(data, usertype);
    }

    @TypeConverter
    public static String proposalStateToJson(ProposalState dataObject){
        Gson gson = new Gson();
        return gson.toJson(dataObject);
    }

}
