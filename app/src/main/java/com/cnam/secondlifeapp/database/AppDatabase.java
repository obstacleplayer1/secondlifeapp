package com.cnam.secondlifeapp.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.cnam.secondlifeapp.JsonConverter.JsonConverter;
import com.cnam.secondlifeapp.model.Game;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Opinion;
import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.model.User;
import com.cnam.secondlifeapp.Dao.GameDao;
import com.cnam.secondlifeapp.Dao.OffersDao;
import com.cnam.secondlifeapp.Dao.OpinionDao;
import com.cnam.secondlifeapp.Dao.ProductDao;
import com.cnam.secondlifeapp.Dao.ProposalDao;
import com.cnam.secondlifeapp.Dao.UserDao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {User.class, Game.class, Offer.class, Opinion.class, Proposal.class, Product.class},version = 1)
@TypeConverters({JsonConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract GameDao gameDao();
    public abstract OffersDao offersDao();
    public abstract ProposalDao proposalsDao();
    public abstract OpinionDao opinionDao();
    public abstract ProductDao productDao();
    private static volatile AppDatabase instance;
    private static final int NUMBER_OF_THREAD = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newScheduledThreadPool(NUMBER_OF_THREAD);

    public static synchronized AppDatabase getDatabase(final Context context){
        if(instance == null)
        {
            synchronized (AppDatabase.class)
            {
                if(instance == null)
                {
                    instance = Room.databaseBuilder
                            (context.getApplicationContext(),AppDatabase.class,
                            "appdatabase")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return instance;
    }



    private static final RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            databaseWriteExecutor.execute(() -> {

                UserDao dao = instance.userDao();
                dao.deleteAll();


                ProductDao productDao = instance.productDao();
                productDao.deleteAll();
                Product product = new Product(1,"edede",545,"dzdzdz","dzdzdz",null);

                productDao.insert(product);
            });
        }
    };

}
