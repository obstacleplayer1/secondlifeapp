package com.cnam.secondlifeapp.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.Dao.OpinionDao;
import com.cnam.secondlifeapp.Dao.UserDao;
import com.cnam.secondlifeapp.database.AppDatabase;
import com.cnam.secondlifeapp.model.Opinion;
import com.cnam.secondlifeapp.model.User;

import java.util.List;

public class OpinionRepository {

    private final OpinionDao opinionDao;
    private final LiveData<List<Opinion>> listLiveOpinionData;

    public OpinionRepository(Application application){
        AppDatabase db = AppDatabase.getDatabase(application);
        opinionDao = db.opinionDao();
        listLiveOpinionData = opinionDao.getAll();
    }

    public LiveData<List<Opinion>> getAllOpinion() { return listLiveOpinionData;}

    public void insert(Opinion opinion){
        new OpinionRepository.InsertOpinionAsyncTask(opinionDao).execute(opinion);
    }


    public void update(Opinion opinion){
        new OpinionRepository.UpdateOpinionAsyncTask(opinionDao).execute(opinion);
    }

    public void delete(Opinion opinion){
        new OpinionRepository.DeleteOpinionAsyncTask(opinionDao).execute(opinion);
    }

    public void deleteAll(){
        new OpinionRepository.DeleteAllOpinionAsyncTask(opinionDao).execute();
    }





    private static class InsertOpinionAsyncTask extends AsyncTask<Opinion, Void, Void> {
        private final OpinionDao opinionDao;

        private InsertOpinionAsyncTask(OpinionDao opinionDao) {
            this.opinionDao = opinionDao;
        }


        @Override
        protected Void doInBackground(Opinion... opinion) {
            opinionDao.insert(opinion[0]);
            return null;
        }
    }

    private static class UpdateOpinionAsyncTask extends AsyncTask<Opinion, Void, Void> {
        private final OpinionDao opinionDao;

        private UpdateOpinionAsyncTask(OpinionDao opinionDao) {
            this.opinionDao = opinionDao;
        }


        @Override
        protected Void doInBackground(Opinion... opinion) {
            opinionDao.update(opinion[0]);
            return null;
        }
    }
    private static class DeleteOpinionAsyncTask extends AsyncTask<Opinion, Void, Void> {
        private final OpinionDao opinionDao;

        private DeleteOpinionAsyncTask(OpinionDao opinionDao) {
            this.opinionDao = opinionDao;
        }


        @Override
        protected Void doInBackground(Opinion... opinion) {
            opinionDao.delete(opinion[0]);
            return null;
        }
    }
    private static class DeleteAllOpinionAsyncTask extends AsyncTask<Void, Void, Void> {
        private final OpinionDao opinionDao;

        private DeleteAllOpinionAsyncTask(OpinionDao opinionDao) {
            this.opinionDao = opinionDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            opinionDao.deleteAll();
            return null;
        }
    }

}
