package com.cnam.secondlifeapp.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.Dao.ProposalDao;
import com.cnam.secondlifeapp.database.AppDatabase;
import com.cnam.secondlifeapp.model.Proposal;
;

import java.util.List;

public class ProposalRepository {
    private final ProposalDao proposalsDao;
    private final LiveData<List<Proposal>> listLiveProposalData;

    public ProposalRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        proposalsDao = db.proposalsDao();
        listLiveProposalData = proposalsDao.getAll();
    }

    public LiveData<List<Proposal>> getAllProposal(){
        return listLiveProposalData;
    }

    public void insert(Proposal proposals){
        new ProposalRepository.InsertProposalAsyncTask(proposalsDao).execute(proposals);
    }


    public void update(Proposal proposals){
        new ProposalRepository.UpdateProposalAsyncTask(proposalsDao).execute(proposals);
    }

    public void delete(Proposal proposals){
        new ProposalRepository.DeleteProposalAsyncTask(proposalsDao).execute(proposals);

    }

    public void deleteAll(){
        new ProposalRepository.DeleteProposalAsyncTask(proposalsDao).execute();
    }

    private static class InsertProposalAsyncTask extends AsyncTask<Proposal, Void, Void> {
        private final ProposalDao proposalsDao;

        private InsertProposalAsyncTask(ProposalDao proposalsDao) {
            this.proposalsDao = proposalsDao;
        }


        @Override
        protected Void doInBackground(Proposal... proposals) {
            proposalsDao.insert(proposals[0]);
            return  null;
        }
    }

    private static class UpdateProposalAsyncTask extends AsyncTask<Proposal, Void, Void> {
        private final ProposalDao proposalsDao;



        private UpdateProposalAsyncTask(ProposalDao proposalsDao) {
            this.proposalsDao = proposalsDao;
        }

        @Override
        protected Void doInBackground(Proposal... proposals) {
            proposalsDao.update(proposals[0]);
            return  null;
        }
    }

    private static class DeleteProposalAsyncTask extends AsyncTask<Proposal, Void, Void> {
        private final ProposalDao proposalsDao;

        private DeleteProposalAsyncTask(ProposalDao proposalsDao) {
            this.proposalsDao = proposalsDao;
        }


        @Override
        protected Void doInBackground(Proposal... proposals) {
            proposalsDao.delete(proposals[0]);
            return  null;
        }
    }

    private static class DeleteAllProposalAsyncTask extends AsyncTask<Void, Void, Void> {
        private final ProposalDao proposalsDao;

        private DeleteAllProposalAsyncTask(ProposalDao proposalsDao) {
            this.proposalsDao = proposalsDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            proposalsDao.deleteAll();
            return  null;
        }
    }



}
