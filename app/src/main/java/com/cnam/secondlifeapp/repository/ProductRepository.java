package com.cnam.secondlifeapp.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.Dao.ProductDao;
import com.cnam.secondlifeapp.Dao.ProposalDao;
import com.cnam.secondlifeapp.database.AppDatabase;
import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.model.Proposal;

import java.util.List;

public class ProductRepository {
    private final ProductDao productDao;
    private final LiveData<List<Product>> listLiveProductData;

    public ProductRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        productDao = db.productDao();
        listLiveProductData = productDao.getAll();

    }

    public LiveData<List<Product>> getAllProduct(){
        return listLiveProductData;
    }

    public void insert(Product product){
        new ProductRepository.InsertProductAsyncTask(productDao).execute(product);
    }


    public void update(Product product){
        new ProductRepository.UpdateProductAsyncTask(productDao).execute(product);
    }

    public void delete(Product product){
        new ProductRepository.DeleteProductAsyncTask(productDao).execute(product);

    }

    public void deleteAll(){
        new ProductRepository.DeleteAllProductAsyncTask(productDao).execute();
    }

    private static class InsertProductAsyncTask extends AsyncTask<Product, Void, Void> {
        private final ProductDao productDao;

        public InsertProductAsyncTask(ProductDao productDao) {
            this.productDao = productDao;
        }

        @Override
        protected Void doInBackground(Product... product) {
            productDao.insert(product[0]);
            return null;

        }
    }
    private static class UpdateProductAsyncTask extends AsyncTask<Product, Void, Void> {
        private final ProductDao productDao;

        public UpdateProductAsyncTask(ProductDao productDao) {
            this.productDao = productDao;
        }

        @Override
        protected Void doInBackground(Product... product) {
            productDao.update(product[0]);
            return null;

        }
    }
    private static class DeleteProductAsyncTask extends AsyncTask<Product, Void, Void> {
        private final ProductDao productDao;

        public DeleteProductAsyncTask(ProductDao productDao) {
            this.productDao = productDao;
        }

        @Override
        protected Void doInBackground(Product... product) {
            productDao.insert(product[0]);
            return null;

        }
    }
    private static class DeleteAllProductAsyncTask extends AsyncTask<Void, Void, Void> {
        private final ProductDao productDao;

        public DeleteAllProductAsyncTask(ProductDao productDao) {
            this.productDao = productDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            productDao.deleteAll();
            return null;

        }
    }


}
