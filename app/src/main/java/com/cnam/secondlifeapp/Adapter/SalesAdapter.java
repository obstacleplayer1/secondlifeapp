package com.cnam.secondlifeapp.Adapter;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.network.RetrofitService;
import com.cnam.secondlifeapp.ui.proposal.ProposalFragment;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.SalesViewHolder> {

    private List<Offer> salesList;



    public SalesAdapter(List<Offer> salesList) { this.salesList = salesList; }

    public Offer getSales(int position){
        return salesList.get(position);
    }

    @NonNull
    @Override
    public SalesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.sales_item, parent, false);
        return new SalesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesViewHolder holder, int position) {
        Offer offer = salesList.get(position);

        String title = offer.getTitle();
        String gameName = offer.getTitle();
        float salesBasePrice = offer.getBasePrice();

        Picasso.get().load(offer.getProducts().get(0).getPictureUrl()).fit().into(holder.salesPicture);
        holder.titleSalesItem.setText(title);
        holder.gameNameItem.setText(gameName);

        holder.salesBasePriceItem.setText(String.format("Prix%s", salesBasePrice));


        holder.buttonDelete.setOnClickListener(v -> {
            MainActivity mainActivity = (MainActivity) v.getContext();
            String token = mainActivity.getToken();

            Call<Offer> listCall = RetrofitService.auth().deleteSales("Bearer "+ token,offer.getId());

            listCall.enqueue(new Callback<Offer>() {
                @Override
                public void onResponse(Call<Offer> call, Response<Offer> response) {
                    if(response.isSuccessful()){
                        Offer offerResponse = response.body();
                        Log.d("ok ", "Supprimé");
                    }
                }

                @Override
                public void onFailure(Call<Offer> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        });

        holder.buttonVoir.setOnClickListener(v -> {
            AppCompatActivity activity = (AppCompatActivity)v.getContext();
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

            ProposalFragment proposalFragment = new ProposalFragment();

            Bundle bundle = new Bundle();

            bundle.putString("pictureUrl", offer.getProducts().get(0).getPictureUrl());
            bundle.putSerializable("offer", (Serializable) offer);
            proposalFragment.setArguments(bundle);
            ft.replace(R.id.fragment_ventes, proposalFragment);
            ft.addToBackStack(null);
            ft.commit();
        });


    }


    @Override
    public int getItemCount() {
        return salesList.size();
    }

    public static class SalesViewHolder extends RecyclerView.ViewHolder{
        TextView titleSalesItem, gameNameItem, salesBasePriceItem;
        ImageView salesPicture;
        Button buttonVoir;
        Button buttonDelete;

        public SalesViewHolder(@NonNull View itemView) {
            super(itemView);
            salesPicture = itemView.findViewById(R.id.salesPicture);
            titleSalesItem = itemView.findViewById(R.id.title_sales_item);
            gameNameItem = itemView.findViewById(R.id.game_name_item);
            salesBasePriceItem = itemView.findViewById(R.id.sales_baseprice_item);
            buttonVoir = itemView.findViewById(R.id.ButtonSales_Proposal);
            buttonDelete = itemView.findViewById(R.id.ButtonSales_Delete);
        }
    }
}
