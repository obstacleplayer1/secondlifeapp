package com.cnam.secondlifeapp.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.ui.gameDetail.GameDetailFragment;
import com.cnam.secondlifeapp.ui.offer.OfferFragment;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.OfferViewHolder> {

    private Context context;
    private Offer offer;

    public OfferAdapter(Context context, Offer offer) {
        this.context = context;
        this.offer = offer;
    }

    @NonNull
    @NotNull
    @Override
    public OfferViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view  = layoutInflater.inflate(R.layout.product_detail_item, parent, false);
        return new OfferViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull OfferViewHolder holder, int position) {
        Product products = offer.getProducts().get(position);

        holder.title_product_item.setText(products.getName());
        holder.product_description.setText(products.getDescription());
        Picasso.get().load(products.getPictureUrl()).into(holder.offer_game_image);

        holder.btn_Detail_Game.setOnClickListener(v -> {
            AppCompatActivity activity = (AppCompatActivity)v.getContext();
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            GameDetailFragment gameDetailFragment = new GameDetailFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable("product", (Serializable) products);
            gameDetailFragment.setArguments(bundle);
            ft.replace(R.id.fragment_offer, gameDetailFragment);
            ft.addToBackStack(null);
            ft.commit();
        });


    }

    @Override
    public int getItemCount() {
        return offer.getProducts().size();
    }


    public class OfferViewHolder extends RecyclerView.ViewHolder{

        TextView  product_description,title_product_item;
        ImageView  offer_game_image;
        Button btn_Detail_Game;
        Button btn_make_offer;
        public OfferViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            title_product_item = itemView.findViewById(R.id.title_product_item);
            product_description = itemView.findViewById(R.id.product_description);
           btn_Detail_Game = itemView.findViewById(R.id.btn_Detail_Game);
            offer_game_image = itemView.findViewById(R.id.offer_game);
            btn_make_offer = itemView.findViewById(R.id.ButtonMakeOffer);
        }
    }
}
