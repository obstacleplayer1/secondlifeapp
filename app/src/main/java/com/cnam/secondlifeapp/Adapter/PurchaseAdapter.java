package com.cnam.secondlifeapp.Adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.ui.buyerProposal.BuyerProposalFragment;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

public class PurchaseAdapter extends RecyclerView.Adapter<PurchaseAdapter.PurchaseViewHolder> {

    private List<Offer> purchaseList;

    public PurchaseAdapter(List<Offer> purchaseList) { this.purchaseList = purchaseList; }

    public Offer getPurchase(int position) { return purchaseList.get(position); }


    @NonNull
    @Override
    public PurchaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.offer_item, parent, false);
        return new PurchaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PurchaseViewHolder holder, int position) {
        Offer purchases = purchaseList.get(position);

        float offerPrice = purchases.getBasePrice();

        holder.offerNameItem.setText(purchases.getTitle());
        holder.offerPriceItem.setText(String.format("Prix : %s",offerPrice));

        Picasso.get().load(purchases.getProducts().get(0).getPictureUrl()).fit().into(holder.pictureOfferItem);


        holder.btnVoir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity activity = (AppCompatActivity)v.getContext();
                FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                BuyerProposalFragment buyerProposalFragment = new BuyerProposalFragment();

                Bundle bundle = new Bundle();
                bundle.putSerializable("offer", (Serializable) purchases);
                buyerProposalFragment.setArguments(bundle);
                ft.replace(R.id.fragment_cart, buyerProposalFragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }

    @Override
    public int getItemCount() { return purchaseList.size(); }

    public static class PurchaseViewHolder extends RecyclerView.ViewHolder{
        TextView offerNameItem, offerPriceItem;
        ImageView pictureOfferItem;
        Button btnVoir;

        public PurchaseViewHolder(@NonNull View itemView){
            super(itemView);
            offerNameItem = itemView.findViewById(R.id.offer_name_item);
            offerPriceItem = itemView.findViewById(R.id.offer_price_item);
            btnVoir = itemView.findViewById(R.id.ButtonOffer_Proposal);
            pictureOfferItem = itemView.findViewById(R.id.picture_offer_item);
        }
    }
}
