package com.cnam.secondlifeapp.Adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.ui.decision.DecisionFragment;

import java.io.Serializable;
import java.util.List;

public class ProposalAdapter extends RecyclerView.Adapter<ProposalAdapter.ProposalViewHolder> {

    private List<Proposal> proposalList;
    private String pictureUrl;

    public ProposalAdapter(List<Proposal> proposalList, String pictureUrl) {
        this.proposalList = proposalList;
        this.pictureUrl = pictureUrl;
    }



    @NonNull
    @Override
    public ProposalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.proposal_item, parent, false);
        return new ProposalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProposalViewHolder holder, int position) {
        Proposal proposal = proposalList.get(position);

        float proposalPrice = proposal.getProposedPrice();


        holder.proposalBuyerItem.setText(proposal.getRequestingUser().getUsername());
        holder.proposalPriceItem.setText(String.format("Prix%s", proposalPrice));
        holder.proposalStatusItem.setText(proposal.getProposalState().toString());



        holder.buttonVoir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity activity = (AppCompatActivity)v.getContext();
                FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                DecisionFragment decisionFragment = new DecisionFragment();

                Bundle bundle = new Bundle();
                bundle.putSerializable("proposal", (Serializable) proposal);
                bundle.putString("pictureUrl", pictureUrl);

                decisionFragment.setArguments(bundle);
                ft.replace(R.id.proposal_fragment, decisionFragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

    }

    @Override
    public int getItemCount() { return proposalList.size(); }

    public static class ProposalViewHolder extends RecyclerView.ViewHolder{
        TextView proposalBuyerItem, proposalPriceItem, proposalStatusItem;
        Button buttonVoir;

        public ProposalViewHolder(@NonNull View itemView) {
            super(itemView);
            proposalBuyerItem = itemView.findViewById(R.id.proposal_buyer_item);
            proposalPriceItem = itemView.findViewById(R.id.proposal_price_item);
            proposalStatusItem = itemView.findViewById(R.id.proposal_status_item);
            buttonVoir = itemView.findViewById(R.id.ButtonProposal_Proposal);
        }
    }
}
