package com.cnam.secondlifeapp.ui.sales;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.Adapter.SalesAdapter;
import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.ViewModel.VentesViewModel;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.network.JsonPlaceHolderApi;
import com.cnam.secondlifeapp.network.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SalesFragment extends Fragment {

    private RecyclerView salesRecycler;
    private static SalesAdapter salesAdapter;
    private RecyclerView.LayoutManager myLayoutManager;
    private static List<Offer> salesList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        VentesViewModel ventesViewModel = new ViewModelProvider(this).get(VentesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_sales, container, false);

        salesRecycler = root.findViewById(R.id.sales_recycler);
        myLayoutManager = new LinearLayoutManager(getContext());

        salesRecycler.setLayoutManager(myLayoutManager);

        getSales();

        salesAdapter = new SalesAdapter(salesList);
        salesRecycler.setAdapter(salesAdapter);

        setHasOptionsMenu(true);

        return root;
    }

    private void getSales() {
        MainActivity mainActivity = (MainActivity) getActivity();
        String token = mainActivity.getToken();

        Call<List<Offer>> listCall = RetrofitService.auth().getSales("Bearer "+ token);

        listCall.enqueue(new Callback<List<Offer>>() {
            @Override
            public void onResponse(Call<List<Offer>> call, Response<List<Offer>> response) {
                List<Offer> offer = response.body();

                if(offer != null){
                    salesList.clear();
                    salesList.addAll(offer);
                    salesAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Offer>> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }



}