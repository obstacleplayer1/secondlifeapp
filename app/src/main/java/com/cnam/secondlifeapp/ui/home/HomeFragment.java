package com.cnam.secondlifeapp.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.cnam.secondlifeapp.Adapter.HomeAdapter;
import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.ViewModel.HomeViewModel;

import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.databinding.FragmentHomeBinding;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.network.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding fragmentHomeBinding;
    private RecyclerView verticalRecycler;
    private HomeAdapter homeAdapter;
    private RecyclerView.LayoutManager myLayoutManager;
    private List<Offer> homeList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeViewModel homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        fragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = fragmentHomeBinding.getRoot();

        verticalRecycler = root.findViewById(R.id.vertical_recycler);
        myLayoutManager = new LinearLayoutManager(getActivity());

        verticalRecycler.setLayoutManager(myLayoutManager);

        getOffer();

        homeAdapter = new HomeAdapter(getContext(),homeList);
        verticalRecycler.setAdapter(homeAdapter);

        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void getOffer() {
        MainActivity mainActivity = (MainActivity) getActivity();
        String token = mainActivity.getToken();

        Call<List<Offer>> listCall = RetrofitService.auth().getOffer( "Bearer "+ token);

        listCall.enqueue(new Callback<List<Offer>>() {
            @Override
            public void onResponse(Call<List<Offer>> call, Response<List<Offer>> response) {

                List<Offer> offer = response.body();

                if(offer != null){
                    homeList.clear();
                    homeList.addAll(offer);
                    homeAdapter.notifyDataSetChanged();
                }

            }
            @Override
            public void onFailure(Call<List<Offer>> call, Throwable t) {
                // textViewResult.setText(t.getMessage());
                t.printStackTrace();
            }
        });

    }

}



