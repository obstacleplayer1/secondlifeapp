package com.cnam.secondlifeapp.ui.decision;

import android.os.Bundle;
import retrofit2.Call;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.ViewModel.DecisionViewModel;
import com.cnam.secondlifeapp.databinding.FragmentDecisionBinding;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.network.JsonPlaceHolderApi;
import com.cnam.secondlifeapp.network.RetrofitService;
import com.cnam.secondlifeapp.ui.home.HomeFragment;
import com.cnam.secondlifeapp.ui.profil.ProfilFragment;
import com.cnam.secondlifeapp.ui.sales.SalesFragment;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class DecisionFragment extends Fragment {

    private TextView price_decision_item;
    private TextInputLayout comment_item;
    private Button buttonDecision_Accept;
    private Button buttonDecision_Refused;
    private Proposal proposal;

    private FragmentDecisionBinding fragmentDecisionBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DecisionViewModel decisionViewModel = new ViewModelProvider(this).get(DecisionViewModel.class);

        Bundle bundle = getArguments();
        Proposal proposal = (Proposal) bundle.getSerializable("proposal");
        String pictureUrl = (String) bundle.getSerializable("pictureUrl");
        fragmentDecisionBinding = FragmentDecisionBinding.inflate(inflater, container, false);
        View view = fragmentDecisionBinding.getRoot();

        ImageView picture_decision_item = fragmentDecisionBinding.pictureDecisionItem;
        price_decision_item = fragmentDecisionBinding.priceDecisionItem;
        TextView buyer_decision_item = fragmentDecisionBinding.buyerDecisionItem;

        comment_item = fragmentDecisionBinding.commentItem;

        buttonDecision_Accept = fragmentDecisionBinding.ButtonDecisionAccept;
        buttonDecision_Refused = fragmentDecisionBinding.ButtonDecisionRefuse;

        buyer_decision_item.setText(proposal.getRequestingUser().getUsername());
        price_decision_item.setText(String.valueOf(proposal.getProposedPrice()));

        Picasso.get().load(pictureUrl).resize(500, 500).into(picture_decision_item);

        buttonDecision_Accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sendComment = Objects.requireNonNull(comment_item.getEditText()).getText().toString();
                sendOffer(proposal.getId(), sendComment);
                Log.d("ok ", "Accepted");
                if(sendComment.equals("")){
                    Toast.makeText(getActivity(),"Comments is Required", Toast.LENGTH_LONG).show();
                }

                AppCompatActivity activity = (AppCompatActivity)v.getContext();
                FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                SalesFragment salesFragment = new SalesFragment();
                ft.replace(R.id.fragment_decision, salesFragment);
                ft.addToBackStack(null);
                ft.commit();
                buttonDecision_Accept.setVisibility(View.INVISIBLE);
                buttonDecision_Refused.setVisibility(View.INVISIBLE);
            }
        });

        buttonDecision_Refused.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                betaRefuseProposal(proposal.getId());
                Log.d("ok", "Refused");

                AppCompatActivity activity = (AppCompatActivity)v.getContext();
                FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                SalesFragment salesFragment = new SalesFragment();
                ft.replace(R.id.fragment_decision, salesFragment);
                ft.addToBackStack(null);
                ft.commit();
                buttonDecision_Accept.setVisibility(View.INVISIBLE);
                buttonDecision_Refused.setVisibility(View.INVISIBLE);
            }
        });

        return view;

    }



    public void betaRefuseProposal(int id){
        MainActivity mainActivity = (MainActivity) getActivity();
        String token = mainActivity.getToken();

        Call<Proposal> proposalCall = RetrofitService.auth().cancelProposal("Bearer "+ token,id);
        proposalCall.enqueue(new Callback<Proposal>() {
            @Override
            public void onResponse(Call<Proposal> call, Response<Proposal> response) {
                if(response.isSuccessful()){
                    Proposal proposal = response.body();
                }
            }

            @Override
            public void onFailure(Call<Proposal> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void sendOffer(int id, String sendComment){
        MainActivity mainActivity = (MainActivity) getActivity();
        String token = mainActivity.getToken();

        Call<Proposal> proposalCall = RetrofitService.auth().acceptProposal("Bearer "+ token,id,sendComment);
        proposalCall.enqueue(new Callback<Proposal>() {
            @Override
            public void onResponse(Call<Proposal> call, Response<Proposal> response) {
                if(response.isSuccessful()){
                    Proposal proposal = response.body();
                }
            }

            @Override
            public void onFailure(Call<Proposal> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
