package com.cnam.secondlifeapp.ui.proposal;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.Adapter.ProposalAdapter;
import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.databinding.FragmentDecisionBinding;
import com.cnam.secondlifeapp.databinding.FragmentProposalBinding;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.network.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProposalFragment extends Fragment {

    private RecyclerView proposalRecycler;
    private static ProposalAdapter proposalAdapter;
    private RecyclerView.LayoutManager myLayoutManager;
    private static List<Proposal> proposalList = new ArrayList<>();

    private FragmentProposalBinding fragmentProposalBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentProposalBinding = FragmentProposalBinding.inflate(inflater, container, false);
        View root = fragmentProposalBinding.getRoot();

        Bundle bundle = getArguments();

        String pictureUrl = (String) bundle.getSerializable("pictureUrl");
        Offer offer = (Offer) bundle.getSerializable("offer");

        proposalRecycler = root.findViewById(R.id.proposal_recycler);
        myLayoutManager = new LinearLayoutManager(getContext());

        proposalRecycler.setLayoutManager(myLayoutManager);

        proposalAdapter = new ProposalAdapter(proposalList, pictureUrl);
        proposalRecycler.setAdapter(proposalAdapter);

        getProposalOfferById(offer.getId());
        setHasOptionsMenu(true);

        return root;
    }

    private void getProposalOfferById(int id) {
        MainActivity mainActivity = (MainActivity) getActivity();
        String token = mainActivity.getToken();

        Call<List<Proposal>> proposalCall = RetrofitService.auth().myProposalOfferById(id,"Bearer "+ token);

        proposalCall.enqueue(new Callback<List<Proposal>>() {
            @Override
            public void onResponse(Call<List<Proposal>> call, Response<List<Proposal>> response) {
                List<Proposal> proposal = response.body();

                if(proposal != null){
                    proposalList.clear();
                    proposalList.addAll(proposal);
                    proposalAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Proposal>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


}