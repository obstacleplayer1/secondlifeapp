package com.cnam.secondlifeapp.ui.buyerProposal;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.databinding.FragmentBuyerproposalBinding;
import com.cnam.secondlifeapp.model.Offer;
import com.squareup.picasso.Picasso;

public class BuyerProposalFragment extends Fragment {

    private TextView title_buyerproposal_item, offer_buyerproposal_item;
    private ImageView imageView2;

    private FragmentBuyerproposalBinding buyerproposalBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View root = inflater.inflate(R.layout.fragment_buyerproposal, container, false);

        Bundle bundle = getArguments();
        Offer offer  = (Offer) bundle.getSerializable("offer");
        buyerproposalBinding = FragmentBuyerproposalBinding.inflate(inflater, container, false);
        title_buyerproposal_item = buyerproposalBinding.titleBuyerproposalItem;
        offer_buyerproposal_item = buyerproposalBinding.offerBuyerproposalItem;
        imageView2 = buyerproposalBinding.imageView2;

        title_buyerproposal_item.setText(offer.getTitle());
        offer_buyerproposal_item.setText(String.format("Prix:%s",offer.getBasePrice()));
        Picasso.get().load(offer.getProducts().get(0).getPictureUrl()).resize(500,500).into(imageView2);

        return buyerproposalBinding.getRoot();
    }
}
