package com.cnam.secondlifeapp.ui.gameDetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.databinding.FragmentGameDetailBinding;
import com.cnam.secondlifeapp.model.Game;
import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.network.RetrofitService;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GameDetailFragment extends Fragment {

    private FragmentGameDetailBinding fragmentGameDetailBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
       fragmentGameDetailBinding = fragmentGameDetailBinding.inflate(inflater, container, false);
       View view = fragmentGameDetailBinding.getRoot();

        Bundle bundle = getArguments();
        Product product = (Product) bundle.getSerializable("product");


        ImageView gameDetailPicture = fragmentGameDetailBinding.gameDetailPicture;
        TextView score = fragmentGameDetailBinding.gameNameScore;
        TextView gameNameDescription = fragmentGameDetailBinding.gameNameDescription;
        TextView gameDetailName = fragmentGameDetailBinding.gameDetailName;

        gameDetailName.setText(product.getName());
        score.setText(String.format("Score : %s", product.getGame().getScore()));
        Picasso.get().load(product.getPictureUrl()).resize(500,500).into(gameDetailPicture);
        gameNameDescription.setText(product.getGame().getDescription());





        return view;
    }






}
