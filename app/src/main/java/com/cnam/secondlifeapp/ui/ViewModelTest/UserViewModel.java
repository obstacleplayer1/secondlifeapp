package com.cnam.secondlifeapp.ui.ViewModelTest;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.cnam.secondlifeapp.database.AppDatabase;
import com.cnam.secondlifeapp.model.User;

import java.util.List;

public class UserViewModel extends AndroidViewModel {

    private final LiveData<List<User>> getListLiveData;
    private AppDatabase appDatabase;

    public UserViewModel(Application application) {
        super(application);
        appDatabase = AppDatabase.getDatabase(this.getApplication());
        getListLiveData = appDatabase.userDao().getAll();
    }

    public LiveData<List<User>> getAllUser() {
        return getListLiveData;
    }


}
